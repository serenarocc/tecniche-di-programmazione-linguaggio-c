#include<stdlib.h>
#include<stdio.h>

#define R 50 //dimensione max della matrice 50x50
#define C 50

typedef struct {
    int r;
    int c;
} dim; //struttura dove salvo la prima riga del file che indica la dim della matrice

typedef struct {
    int b;
    int h;
    int x;
    int y;
} rett; //struct che indica tuttto ciò che c'è da sapere di un rettangolo: base, altezza e coordinate x e y


dim leggiMatrice(int mat[][C]);
void stampaMatrice(int mat[][C], dim dimensioni);
rett riconosciRegione(int mat[][C], dim dimensioni, int r, int c);

int main(int argc, char **argv) {
    int M[R][C], i, j;
    dim dimensioni;//variabile di tipo dim
    rett rettangolo, rettH, rettB, rettA;//variabili di tipo rettangolo
    rettH.x = rettH.y = rettA.x = rettA.y = rettB.x = rettB.y = -1;//inizializzo tutte le coordinate delle variabili di tipo rettangolo

    dimensioni = leggiMatrice(M);//leggo la matrice da file e la salvo. Riporto la variabile di tipo struct che contiene nr e nc
    stampaMatrice(M, dimensioni);//stampo la matrice che ho appena acquisito

    for (i=0; i<dimensioni.r; i++) {//itero per la dim di nr e nc
        for(j=0;j<dimensioni.c;j++) {
            rettangolo.b = 0;//inizializzo
            rettangolo.h = 0;
            rettangolo = riconosciRegione(M, dimensioni, i, j);
            if (rettangolo.b!=0 && rettangolo.h!=0) {
                /* Aggiorna (eventualmente) le regioni "migliori" per poi stampare a termine scansione */
                if (rettB.x == -1 || rettB.b < rettangolo.b) rettB = rettangolo;
                if (rettA.x == -1 || (rettA.b * rettA.h) < (rettangolo.b * rettangolo.h)) rettA = rettangolo;
                if (rettH.x == -1 || rettH.h < rettangolo.h) rettH = rettangolo;
            }
        }
    }
    printf("Max Base: estr. sup. SX = <%d,%d> b=%d, h=%d, Area=%d\n", rettB.x, rettB.y, rettB.b, rettB.h, rettB.b*rettB.h);
    printf("Max Area: estr. sup. SX = <%d,%d> b=%d, h=%d, Area=%d\n", rettA.x, rettA.y, rettA.b, rettA.h, rettA.b*rettA.h);
    printf("Max Altezza: estr. sup. SX = <%d,%d> b=%d, h=%d, Area=%d\n", rettH.x, rettH.y, rettH.b, rettH.h, rettH.b*rettH.h);

    return 0;
}

dim leggiMatrice(int mat[][C]) {//da parametro passo la matrice
    int i, j, nr, nc;
    dim dimensioni; //variabile di tipo struttura che conterrà la dim della matrice
    FILE *in = fopen("../mappa.txt", "r");//apro file in lettura
    if (in == NULL) //controllo corretta apertura file
        exit(-1);

    fscanf(in, "%d %d", &nr, &nc); //acquisisco da file la prima riga dove indica le dim della matrice
    for (i=0; i<nr; i++)
        for (j=0; j<nc; j++)
            fscanf(in, "%d", &mat[i][j]);//faccio ciclo per leggere la matrice e salvarla
    dimensioni.r = nr;//salvo nr e nc nella struttura dim
    dimensioni.c = nc;
    fclose(in);//chiudo file
    return dimensioni; //riporto la variabile di tipo struttura
}

void stampaMatrice(int mat[][C], dim dimensioni) {//passo come parametro la matrice e la variabile dimensione di tipo struct che contiene nr e nc
    int i, j;
    for (i=0; i<dimensioni.r; i++) { //ciclo per stampare l a matrice di dim nr e nc
        for (j=0; j<dimensioni.c; j++)
            printf("%d ", mat[i][j]);
        printf("\n");
    }
    return;
}

rett riconosciRegione(int mat[][C], dim dimensioni, int r, int c) {
    int x, y, b=0, h=0;
    rett rettangolo;//variabile di tipo struttura

    if (mat[r][c] != 1 || (r>0 && (mat[r-1][c] == 1)) || (c>0 && (mat[r][c-1] == 1))) {
        rettangolo.b = b;
        rettangolo.h = h;
        return rettangolo;
    }
    /* si tratta di angolo in alto a sinistra di un rettangolo */
    x = r;
    y = c;
    while (y<dimensioni.c && mat[x][y] == 1)
        y++;
    b = y - c;
    y = c;
    while (x<dimensioni.r && mat[x][y] == 1)
        x++;
    h = x - r;

    rettangolo.b = b;
    rettangolo.h = h;
    rettangolo.x = r;
    rettangolo.y = c;

    return rettangolo;
}

