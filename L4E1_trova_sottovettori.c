#include<stdlib.h>
#include<string.h>
#include<stdio.h>

#define MAXN 30


void stampaIntervalloVettore(int v[MAXN], int r, int l) ;

int cerca1(int v[MAXN], int n) { //METODO 1
    int lung, j, k;
    int daStampare, trovato;

    trovato = 0;
    for(lung=n; !trovato && lung>0; lung--) { /* per ogni lunghezza decrescente dei sottovettori non minore della massima sinora trovata*/
        for(j=0; j<=n-lung; j++) { /* per ogni possibile indice di inizio di un sottovettore */
            daStampare = 1;
            for(k=j; daStampare && k<j+lung; k++) { /* verifica di validita' del sottovettore */
                if (v[k] == 0) {
                    daStampare = 0; /* sottovettore non valido */
                }
            }
            if (daStampare) {
                trovato = 1; /* imposta il flag che non lascera' iterare su una lunghezza minore */
                printf("sottovettore massimo: ");
                stampaIntervalloVettore(v,j,j+lung-1);
                j = k+1; /* saltiamo il sottointervallo appena controllato */
            }
        }
    }
    return 0;
}

int cerca2(int v[MAXN], int n) { //METODO 2
    int maxl=0, i, i0=-1;

    /* trova la lunghezza dei sottovettori di lunghezza massima */
    for(i=0; i<=n; i++) {
        if (i<n && v[i]!=0 && (i==0 || v[i-1]==0)) {
            /* indice di inizio del sottovettore */
            i0 = i;
        }
        else if (i>0 && (i==n || v[i]==0) && v[i-1]!=0) {
            /* il sottovettore e' terminato, ne calcolo la lunghezza e la confronto con il massimo temporaneo, eventualmente aggiornandolo */
            if (i-i0 > maxl) {
                maxl = i-i0;
            }
        }
    }
    /* conoscendo la lunghezza massima, stampa i corrispondenti sottovettori (eventualmente più di uno) */
    for(i=0; i<=n; i++) {
        if (i<n && v[i]!=0 && (i==0 || v[i-1]==0)) {
            /* indice di inizio del sottovettore */
            i0 = i;
        }
        else if (i>0 && (i==n || v[i]==0) && v[i-1]!=0) {
            /* il sottovettore e' terminato, se e' di lunghezza massima lo stampo */
            if (i-i0 == maxl) {
                printf("sottovettore massimo: ");
                stampaIntervalloVettore(v,i0,i0+maxl-1);
            }
        }
    }

    return 0;
}

int cerca3(int v[MAXN], int n) {  //METODO 3
    int maxl=0, nmax=0, i, i0=-1;
    int massimi_i0[MAXN];

    /* identifica i sottovettori di lunghezza massima */
    for(i=0; i<=n; i++) {
        if (i<n && v[i]!=0 && (i==0 || v[i-1]==0)) {
            /* indice di inizio del sottovettore */
            i0 = i;
        }
        else if (i>0 && (i==n || v[i]==0) && v[i-1]!=0) {
            /* il sottovettore e' terminato, ne calcolo la lunghezza e la confronto con il massimo temporaneo,
              eventualmente aggiornando il massimo, il vettore massimi_i0 e il suo indice nmax */
            if (i-i0 > maxl) {
                maxl = i-i0;
                massimi_i0[0] = i0;
                nmax=1;
            }
            else if (i-i0 == maxl) {
                /* il sottovettore trovato e' di lunghezza massima. Accumulo in massimi_i0 il suo indice di partenza */
                massimi_i0[nmax++] = i0;
            }
        }
    }
    /* stampa i sottovettori a massima lunghezza  (eventualmente più di uno)  */
    for(i=0; i<nmax; i++) {
        printf("sottovettore massimo: ");
        stampaIntervalloVettore(v,massimi_i0[i],massimi_i0[i]+maxl-1);
    }

    return 0;
}

int main(int argc, char *argv[]) {
    int vett[MAXN];

    int i, n, selettore = 1;
/*il selettore serve per indicare il metodo di ricerca da utilizzare. In questo caso
 * è posto al metodo 1 ma può indicare qualsiasi altro metodo.*/


    printf("Quanti elementi da 1 a %d vuou metttere nel vettore:\n", MAXN);
    scanf("%d", &n);
    printf("Inserisci gli elementi del vettore:\n ");

    if (n>0 && n<=MAXN) { /*controllo che il num di elem N sia valido*/
        for(i=0; i<n; i++) {/*acquisisco da tastiera gl elem del vettore*/
            scanf("%d", &vett[i]);
        }
        }

   /* else {
        n = 17;
    }*/

    printf("Il vettore e':\n");
    stampaIntervalloVettore(vett,0,n-1);

    switch (selettore) { //TUTTI I METODI
        case 1: cerca1(vett, n);  //METODO 1
            break;
        case 2: cerca2(vett, n); //METODO 2
            break;
        case 3: cerca3(vett, n); //METODO 3
            break;
        default: printf("errore: selettore errato\n");
    }

    return 0;
}

void stampaIntervalloVettore(int v[MAXN], int r, int l) {
    int i;
    for(i=r; i<=l; i++){
        printf("%d ", v[i]);
    }
    printf("\n");
}

