# Tecniche di programmazione - linguaggio C /Programming Techniques
La cartella Tecniche di programmazione - linguaggio C
contiene  lo svolgimento dei laboratori del corso di Tecniche di programmazione del Politecnico di Torino dell'anno 2022.

Il corso Tecniche di prorammazione è propedeutico al corso Algoritmi e strutture dati, il cui materiale si può sempre trovare tra i miei progetti personali.

Linguaggio utilizzato: C.

Gli ide impiegati sono CLion 2022.2 e Code::Blocks 17.12.


-----------------------------------------------------------------------

The Programming Techniques directory contains some projects carried out during the course of Programming Techniques at the Politecnico di Torino in 2022.

The course Programming Techniques is preparatory to the course Algorithms and Data Structures, whose material can always be found among my personal projects.

Language used: C.

Ide used are: CLion 2022.2 and Code::Blocks 17.12.

## Link ad altri miei progetti personali / Links to other personal projects

- [ ] [Tecniche di Programmazione/ Programming Techniques](https://gitlab.com/serenarocc/tecniche-di-programmazione-linguaggio-c)
- [ ] [Algoritmi linguaggio C / Algorithms](https://gitlab.com/serenarocc/algoritmi-linguaggio-c)
- [ ] [Algoritmi e Strutture Dati / Algorithms and Data Structures](https://gitlab.com/serenarocc/algoritmi-e-strutture-dati-linguaggio-c)
- [ ] [Sistemi Operativi/ Operating Systems](https://gitlab.com/serenarocc/sistemi-operativi) 
- [ ] [Calcolatori Elettronici/Computer architecture](https://gitlab.com/serenarocc/calcolatori-elettronici-linguaggio-assembly-mips)
- [ ] [Basi di Dati /Databases](https://gitlab.com/serenarocc/basi-di-dati)
- [ ] [Programmazione a oggetti /Object Oriented Programming](https://gitlab.com/serenarocc/object-oriented-programming-java)
