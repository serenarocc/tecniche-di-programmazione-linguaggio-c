#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define LUNG 20
void codifica(FILE *fin, FILE *fout);
//int decodifica(FILE *fin, FILE *fout);

int main() {
    FILE *fin, *fout;
    char istruzione, finiziale[LUNG], foutput[LUNG];
    int v;
    printf("Digita C se vuoi fare una codifica, digita D se vuoi fare una decodifica:\n");
    scanf("%c",&istruzione);

    printf("Digitare nome file input nella versione ../nome.txt:");
    scanf("%s",finiziale);

    printf("Digita nome file output nella versione ../nome.txt:");
    scanf("%s",foutput);

    if(istruzione=='C') {

        fin = fopen(finiziale, "r");
        fout = fopen(foutput, "w");
        if (fin == NULL || fout == NULL) exit(-1);//controllo apertura corretta
        codifica(fin, fout);

        /* if(istruzione=='D'){

             fin = fopen(finiziale, "w");
             fout = fopen(foutput, "r");
             if (fin==NULL || fout==NULL ) exit(-1);
             v=decodifica(fin,fout);
         }*/
    }
        else{
            printf("Istruzione di codifica e decofic errate\n");
            return 2;
        }


    fclose(fin);
    fclose(fout);
    return 0;
}


void codifica(FILE *fin, FILE *fout){
    char c, c0, newc,h=0;
    int k=0;
    int precedente_num=0;
    c0=fgetc(fin); //primo carattere
    fprintf(fout, "%c",c0);/*xke non è preceduto da niente*/

    do {
        c=fgetc(fin);
        if(c>'0'&& c<'9'){
            newc=c+k;
            if(newc>9){newc=0;}
            k++;
            fprintf(fout, "%c",newc);
            c0=c;
            precedente_num=1;
        }
        if(c0>'0'&& c0<'9'){ //non cambia nulla xke carattere prima è un numero
            fprintf(fout, "%c",c);
        }
        else{
            if(c0>='A' && c0<='Z')//carattere prima è maiuscolo
            {
                h=c0-'A';
                //manca il controllo che il modulo sia max 26
                fprintf(fout, "%c",h);
                c0=c;
            }
            if(c0>='a' && c0<='z')//carattere prima è minuscolo
            {
                h=c0-'a';
                fprintf(fout, "%c",h);
                c0=c;
            }

        }
    } while(c!=EOF);


    }
