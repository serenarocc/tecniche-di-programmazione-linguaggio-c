#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#define MAX_PAROLA 26 //dim max ogni parola nel testo
#define N 20

#define filein "../testo.txt"
#define filesq "../sequenze.txt"

int cercaParola(char seq[MAX_PAROLA]);
int leggiParola(FILE *fp, char parola[MAX_PAROLA]);

int main() {
    int i, n;
    char seq[MAX_PAROLA];
    FILE *fsq;

    fsq = fopen(filesq, "r");//apro file sequenza
    if (fsq == NULL)//cotrollo corretta apertura file
        exit(-1);

    fscanf(fsq, "%d", &n);//in n ho il num di righe del file sequenza.txt

    for (i = 0; i < n; i++) {//ripeto per tutte le parole da ricercare
        fscanf(fsq, "%s", seq); //prendo la prima parola da ricercare del file testo.txt
        printf("cerco: %s\n", seq);
        if (cercaParola(seq)==0) {
            printf("sequenza non trovata\n");
        }
    }

    fclose(fsq);  //chiusura file sequenza.txt

    return 0;
}
//controllo che la seq sia nella parola
int stristr(char *s1, char *s2) { //s1=parola, s2=sequenza
    int i = 0, j = 0;

    while(s1[i] != '\0'  && s2[j] != '\0') {
        if(tolower(s1[i]) == tolower(s2[j])) {
            j++ ;
        } else {
            j = 0 ;
        }
        i++;
    }
    return (s2[j] == '\0') ;
}

int cercaParola(char seq[MAX_PAROLA]) {
    FILE *fp;
    char parola[MAX_PAROLA];//parola generica che devo scandire carattere per carattere per trovare l'occorrrenza
    int i, conta; //conta serve per contare quante parole ci sono nel testo

    fp = fopen(filein, "r");//apro file testo.txt
    if (fp == NULL) {//controllo corretta apertura file
        printf("Errore apertura file delle parole\n");
        exit(-1);
    }

    for (i=1,conta=0; leggiParola(fp,parola)>0; i++) { //for fino a quando c'è una parola e non uno spazio o la fine del file
        if (stristr(parola, seq)) {
            printf("\t%s %d\n", parola, i);//ho trovato sequenza
            conta++;
        }
    }

    fclose(fp);

    return conta;
}

int leggiParola(FILE *fp, char parola[MAX_PAROLA]) {
    int i;
    // cerca primo carattere alfanumerico
    do {
        fscanf(fp, "%c", &parola[0]);//prendo carattere
    } while (!feof(fp) && !isalnum(parola[0]));

    /*La funzione isalnum() controlla se l'argomento passato è un carattere alfanumerico (alfabeto o numero) o meno.*/

    if (feof(fp)) return 0; //fine file

    // cerca primo carattere non alfanumerico
    i=0;
    do {
        fscanf(fp, "%c", &parola[++i]);
    } while (!feof(fp) && isalnum(parola[i]));
    // aggiungi fine riga
    parola[i] = '\0';
    //  printf("PAROLA: %s\n", parola);
    return i;
}

