#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int print_string(FILE *fp, char build_string[], int counter);
int end_punct(char c);

int main() {

    FILE *fp_read, *fp_write;
    char build_string[25+1]; //stringa che corrisponde alla riga, max 25 caratteri
    char c = 'a';
    int counter = 0, flag_p = 0, flag_ep = 0;

    if((fp_write = fopen("../output.txt", "w"))==NULL){ //apertura file
        printf(("\n Errore apertura output.txt ..."));
        return -1;
    }
     if((fp_read = fopen("../input.txt", "r"))==NULL){ //apertura file
         printf("\n Errore apertura input.txt ...");
         return -1;
     }

    while(c!=EOF){//continua fino a quando non arrivi a fine file

        if(counter + 1 == 25){ //se sono arrivato alla max dimensione della stringa
            build_string[counter + 1] = '\0'; //metto a capo
            print_string(fp_write, build_string, counter); //stampo la stringa
            counter = 0;
        }
        if((c = fgetc(fp_read))!=EOF){ //prendo carattere da file input
            if(c == '\n'){ //se input sono arrivato a capo
                build_string[counter] = '\0'; //metto in vett che devo stampare il terminatore di stringa
                print_string(fp_write, build_string, counter); //stampo la stringa in file output
                counter = 0;
            }
            if(c >='0' && c <='9'){ //se è un num lo sostituisco con *
                c = '*';
            }
            if(ispunct(c)){ //La funzione ispunct() controlla se un carattere è un segno di punteggiatura o meno.
                flag_p = 1; //flag ho trovato un punto. carattere dopo un punto deve essere maiuscolo
                if(end_punct(c)){//se carattere è . ! ?
                    flag_ep = 1;
                }
            }

            if(c == ' '){ //se trovo uno spazio flag punto è a 0
                flag_p = 0;
            }

            if(isalpha(c))//La funzione isalpha() controlla se un carattere è alfabetico o meno.
            {
                if(flag_p){ //se al passo prima avevo impostato il flag punto
                    build_string[counter] = ' ';
                    counter++;
                        if(counter + 1 == 25){
                            build_string[counter+1] = '\0';
                            print_string(fp_write, build_string, counter);
                            counter = 0;
                        }
                    flag_p = 0;
                }
                if(flag_ep){ //se al passo prima avevo settato flag per carattere . ! ?
                    c = toupper(c); //carattere dopo va maiuscolo
                    flag_ep = 0;
                }
            }
            if(c!='\n') {
                build_string[counter] = c;
                counter++;
            }
        }
    }

    return 0;
}

int print_string(FILE *fp, char build_string[], int counter){ //stampo la stringa su file di output
    int len = 0;
    int c_letti = counter + 1;
    if(counter + 1 != 25)
    {
        for (counter; counter <= 24; counter++) {
            build_string[counter] = ' '; //aggiungo gli spazi fino a quando non completo la riga
        }
    }
    fprintf(fp, "%s | c:%d \n", build_string, c_letti); //stampo la riga più i caratteri finali che dicono quanti caratteri ho letto a sx
    return 1;
}

int end_punct(char c){
    if(c == '.' || c == '?' || c == '!'){
        return 1;
    } else{
        return 0;
    }
}

